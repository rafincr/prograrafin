﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles test UIs present in the SignIn Scene
/// </summary>

public class UIHandler : MonoBehaviour
{
    public TMP_InputField code;
    public static string tokenID;
    public void OnClickGetGoogleCode()
    {
        GoogleAuthenticator.GetAuthCode();
        code.text = GoogleAuthenticator.code_challenge;
    }

    public void OnClickGoogleSignIn()
    {
        GoogleAuthenticator.ExchangeAuthCodeWithIdToken(code.text, idToken =>
            {
                FirebaseAuthHandler.SingInWithToken(idToken, "google.com");
                tokenID = idToken;
            });
    }
}
